# Installing

```bash
flatpak --user install ephy.flatpak
```

Faster pinch zoom requires the accelerated compositing mode, so we need to enable it:

```bash
flatpak run --command=gsettings org.gnome.Epiphany.Devel//gestures set org.gnome.Epiphany.web:/org/gnome/epiphany/web/ hardware-acceleration-policy 'always'
```

(Use the following command to reset to default if needed)

```bash
flatpak run --command=gsettings org.gnome.Epiphany.Devel//gestures reset org.gnome.Epiphany.web:/org/gnome/epiphany/web/ hardware-acceleration-policy
```

# Running

```bash
flatpak run org.gnome.Epiphany.Devel//gestures
```
